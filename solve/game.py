# `random` module is used to shuffle field, see:
# https://docs.python.org/3/library/random.html#random.shuffle
import random
import sys

# Empty tile, there's only one empty cell on a field:
EMPTY_MARK = 'x'

# Dictionary of possible moves if a form of:
# key -> delta to move the empty tile on a field.
MOVES = {
    'w': -4,
    's': 4,
    'a': -1,
    'd': 1,
}


def shuffle_field():

    """
    This function is used to create a field at the very start of the game.

    :return: list with 16 randomly shuffled tiles,
        one of which is a empty space.
    """

    field = [i for i in range(1, 16)]
    field.append(EMPTY_MARK)

    random.shuffle(field)

    # 'Математическое описание' from wiki
    summ = 0
    for i in range(len(field)):
        if field[i] == EMPTY_MARK:
            summ += i // 4 + 1
            continue
        for k in range(i + 1, len(field)):
            summ += 1 if field[k] != EMPTY_MARK and field[k] < field[i] else 0

    return field if summ % 2 == 0 else shuffle_field()






def print_field(field):

    """
    This function prints field to user.

    :param field: current field state to be printed.
    :return: None
    """

    for i in range(4):
        for j in range(4):
            print(field[i * 4 + j], end=' ')
        print()



def is_game_finished(field):

    """
    This function checks if the game is finished.

    :param field: current field state.
    :return: True if the game is finished, False otherwise.
    """

    return field == ([i for i in range(1, 16)] + [EMPTY_MARK])


def perform_move(field, key):

    """
    Moves empty-tile inside the field.

    :param field: current field state.
    :param key: move direction.
    :return: new field state (after the move)
        or `None` if the move can't me done.
    """

    if key not in MOVES:
        return None


    ind = field.index(EMPTY_MARK)

    if key == 'a' and ind % 4 == 0:
        return None

    if key == 'd' and ind % 4 == 3:
        return None

    if key == 'w' and (ind // 4) == 0:
        return None

    if key == 's' and (ind // 4) == 3:
        return None

    new_pos = ind + MOVES[key]
    field[ind], field[new_pos] = field[new_pos], field[ind]

    return field





def handle_user_input():

    """
    Handles user input.

    List of accepted moves:
        'w' - up,
        's' - down,
        'a' - left,
        'd' - right

    :return: <str> current move.
    """
    return input()


def main():

    """
    The main function. It starts when the program is called.

    It also calls other functions.
    :return: None
    """

    field = shuffle_field()
    print_field(field)
    step = 0

    while not is_game_finished(field):
        key = handle_user_input()
        new_field = perform_move(field, key)
        if new_field == None:
            print("Unacceptable action!")
            continue
        field = new_field
        print_field(field)
        step += 1

    print("Сongratulations you won!")
    print(f"You've taken {step} steps!")




if __name__ == '__main__':
    main()
